using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SFB;

public class ApplyTexturesToModel : MonoBehaviour
{
    #region DECLARATIONS

    [Header("Texture Images Components")]
    [SerializeField] private RawImage AlbedoTextureImage;
    [SerializeField] private RawImage EmissiveTextureImage;
    [SerializeField] private RawImage NormalTextureImage;
    [SerializeField] private RawImage HeightTextureImage;
    [SerializeField] private RawImage GlossDetail2TextureImage;
    [SerializeField] private RawImage MetallicTextureImage;
    [SerializeField] private RawImage OcclusionTextureImage;
    
    // Generic Textures
    [Space()]
    [SerializeField] private Texture2D NoTexture;
    [SerializeField] private List<Texture2D> genericDefaultTextures = new List<Texture2D>();


    // Texture2Ds
    private Texture2D AlbedoTexture;
    private Texture2D EmissiveTexture;
    private Texture2D NormalTexture;
    private Texture2D HeightTexture;
    private Texture2D DetailTexture;
    private Texture2D MetallicTexture;
    private Texture2D OcclusionTexture;
    private Texture2D GlossDetail2Texture;

    // To Stored Textures
    private Texture ModelDefaultAlbedo;
    private Texture ModelDefaultEmissive;
    private Texture ModelDefaultNormal;
    private Texture ModelDefaultHeight;
    private Texture ModelDefaultDetail;
    private Texture ModelDefaultMetallic;
    private Texture ModelDefaultOcclusion;
    private Texture ModelDefaultGlossDetail2;

    // Last Paths
    private string AlbedoLastPath;
    private string EmissiveLastPath;
    private string MetallicLastPath;
    private string NormalLastPath;
    private string HeightLastPath;
    private string DetailLastPath;
    private string OcclusionLastPath;
    private string GlossDetail2LastPath;

    // Misceallenious
    private Material newMaterial;

    #endregion

    #region FIND_TEXTURES

    class TextureDialogBoxOutput
    {
        string path;
        Texture2D texture;

        public string Path { get { return path; } }
        public Texture2D Texture { get { return texture; } }

        public TextureDialogBoxOutput(string path, Texture2D texture)
        {
            this.path = path;
            this.texture = texture;
        }
    }

    // Create and return a Texture2D based on the information
    // picked up via a file browser.
    public TextureDialogBoxOutput FindTextureWithDialogBox(string defaultPath, string extention)
    {
        string fileBrowserPath = StandaloneFileBrowser.OpenFilePanel("Select A Texture...", defaultPath, extention, false)[0];

        if(fileBrowserPath == null)
        {
            Debug.LogWarning("The user didn't select anything or closed the dialog box.");
            return null;
        }

        if(string.IsNullOrEmpty(fileBrowserPath))
        {    
            Debug.LogWarning("The selected texture path is null or empty.");
            return null;
        }

        byte[] imageBytes = System.IO.File.ReadAllBytes(fileBrowserPath))

        Texture2D createdTexture = new Texture2D(2, 2);
        createdTexture.LoadImage(imageBytes);
        createdTexture.Apply();

        TextureDialogBoxOutput createdTextureDialogBoxOutput = new TextureDialogBoxOutput(fileBrowserPath, createdTexture);
        return createdTextureDialogBoxOutput;
        
    }

    // Find textures
    public void FindAlbedoTextureWithDialogBox() { DisplayMaterialTexture(DataCenter.MaterialFields._A); }
    public void FindDetailTextureWithDialogBox() { DisplayMaterialTexture(DataCenter.MaterialFields._D); }
    public void FindEmissiveTextureWithDialogBox() { DisplayMaterialTexture(DataCenter.MaterialFields._E); }
    public void FindHeightTextureWithDialogBox() { DisplayMaterialTexture(DataCenter.MaterialFields._H); }
    public void FindMetallicTextureWithDialogBox() { DisplayMaterialTexture(DataCenter.MaterialFields._M); }
    public void FindNormalTextureWithDialogBox() { DisplayMaterialTexture(DataCenter.MaterialFields._N); }
    public void FindOcclusionTextureWithDialogBox() { DisplayMaterialTexture(DataCenter.MaterialFields._O); }

    #endregion

    #region ADD_TEXTURES

    // Set the fields for materials (see DisplayMaterialTexture method).
    private void SetMaterialTypePropreties(string lastPath, Texture2D texture, RawImage image, string readPath, Texture2D readTexture)
    {
        lastPath = DataCenter.GetPathWithoutFileName(readPath);

        texture = readTexture;
        image.texture = readTexture;   
    }

    // Displays the corresponding material image into it's
    // Image component and into the Texture field.
    public void DisplayMaterialTexture(DataCenter.MaterialFields materialType)
    {
        TextureDialogBoxOutput readTextureDialogBox = FindTextureWithDialogBox(DataCenter.lastFilePath, "png");
        Texture2D readTexture = readTextureDialogBox.Texture;
        string readPath = readTextureDialogBox.Path;

        DataCenter.LastFilePath = readPath;

        switch(materialType)
        {   
            case materalType == DataCenter.MaterialFields._A:
                SetMaterialTypePropreties(AlbedoLastPath, AlbedoTexture, AlbedoTextureImage, readPath, readTexture);
                break;
            case materalType == DataCenter.MaterialFields._D:
                SetMaterialTypePropreties(DetailLastPath, DetailTexture, DetailTextureImage, readPath, readTexture);
                break;
            case materialType = DataCenter.MaterialFields._E:
                SetMaterialTypePropreties(EmissiveLastPath, EmissiveTexture, EmissiveTextureImage, readPath, readTexture);
                break;
            case materialType = DataCenter.MaterialFields._H:
                SetMaterialTypePropreties(HeightLastPath, HeightTexture, HeightTextureImage, readPath, readTexture);
                break;
            case materialType = DataCenter.MaterialFields._M:
                SetMaterialTypePropreties(MetallicLastPath, MetallicTexture, MetallicTextureImage, readPath, readTexture);
                break;
            case materialType = DataCenter.MaterialFields._N:
                SetMaterialTypePropreties(NormalLastPath, NormalTexture, NormalTextureImage, readPath, readTexture);
                break;
            case materialType = DataCenter.MaterialFields._O:
                SetMaterialTypePropreties(OcclusionLastPath, OcclusionTexture, OcclusionTextureImage, readPath, readTexture);
            default:
                Debug.LogError("The input MaterialField is not correct.");
                break;
        }
    }

    private bool AddTexture()
    {
        
    }

    #endregion

    #region REMOVE_TEXTURES
    #endregion

    #region REFRESH_TEXTURES
    #endregion

    #region EXPORT_TEXTURES
    #endregion
}