using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SFB;

public class ApplyTexturesToModel : MonoBehaviour
{
    #region Declarations
    [SerializeField] private RawImage AlbedoTextureImage;
    [SerializeField] private RawImage EmissiveTextureImage;
    [SerializeField] private RawImage NormalTextureImage;
    [SerializeField] private RawImage HeightTextureImage;
    [SerializeField] private RawImage GlossDetail2TextureImage;
    [SerializeField] private RawImage MetallicTextureImage;
    [SerializeField] private RawImage OcclusionTextureImage;

    private Texture2D AlbedoTexture;
    private Texture2D EmissiveTexture;
    private Texture2D NormalTexture;
    private Texture2D HeightTexture;
    private Texture2D DetailTexture;
    private Texture2D MetallicTexture;
    private Texture2D OcclusionTexture;
    private Texture2D GlossDetail2Texture;

    [SerializeField] private Texture2D NoTexture;
    [SerializeField] private List<Texture2D> genericDefaultTextures = new List<Texture2D>();

    private Texture ModelDefaultAlbedo;
    private Texture ModelDefaultEmissive;
    private Texture ModelDefaultNormal;
    private Texture ModelDefaultHeight;
    private Texture ModelDefaultDetail;
    private Texture ModelDefaultMetallic;
    private Texture ModelDefaultOcclusion;
    private Texture ModelDefaultGlossDetail2;

    private string AlbedoLastPath;
    private string EmissiveLastPath;
    private string MetallicLastPath;
    private string NormalLastPath;
    private string HeightLastPath;
    private string DetailLastPath;
    private string OcclusionLastPath;
    private string GlossDetail2LastPath;

    private Material newMaterial;

    #endregion

    private void Start()
    {
        DataCenter.ApplyTexturesToModelScript = this;

        if(!string.IsNullOrEmpty(PlayerPrefs.GetString("DataCenter_LastFilePath")))
        {
            DataCenter.LastFilePath = PlayerPrefs.GetString("DataCenter_LastFilePath");
        }
    }

    public void setModelDefaultTextures(Material modelMaterial)
    {
        string[] mapNames = {"", "_BumpMap", "", "_DetailAlbedoMap" ,
            "_OcclusionMap", "_ParallaxMap", "_DetailMask", "_EmissionMap" };

        DataCenter.ModelUsingGenericTexture[0] = false; // IF MODEL USES TEXTURE LIKE IN GAME
        DataCenter.ModelUsingTexture[0] = true;
        DataCenter.ModelUsingUserSetTexture[0] = false;

        // 1ST
        if (modelMaterial.GetTexture(mapNames[1]) != null)
        {
            DataCenter.ModelUsingGenericTexture[1] = false; // IF MODEL USES TEXTURE LIKE IN GAME
            DataCenter.ModelUsingTexture[1] = true;
            DataCenter.ModelUsingUserSetTexture[1] = false;
        }
        else
        {
            modelMaterial.SetTexture(mapNames[1], genericDefaultTextures[1]);
            DataCenter.ModelUsingGenericTexture[1] = true; // IF MODEL IS INSTEAD USING GENERIC TEXTURE
            DataCenter.ModelUsingTexture[1] = true;
            DataCenter.ModelUsingUserSetTexture[1] = false;
        }

        // 2ND
        if (modelMaterial.GetTexture(DataCenter.UsingSpecularSetup ? "_SpecGlossMap" : "_MetallicGlossMap") != null)
        {
            DataCenter.ModelUsingGenericTexture[2] = false; // IF MODEL USES TEXTURE LIKE IN GAME
            DataCenter.ModelUsingTexture[2] = true;
            DataCenter.ModelUsingUserSetTexture[2] = false;
        }
        else
        {
            modelMaterial.SetTexture(DataCenter.UsingSpecularSetup ? "_SpecGlossMap" : "_MetallicGlossMap", null);
            DataCenter.ModelUsingGenericTexture[2] = false; // IF MODEL IS INSTEAD USING NO TEXTURE
            DataCenter.ModelUsingTexture[2] = false;
            DataCenter.ModelUsingUserSetTexture[2] = false;
        }

        // 3RD+
        for (int i = 3; i < genericDefaultTextures.Count; i++)
        {
            if(i == 6)
            {
                // 6TH
                DataCenter.ModelUsingGenericTexture[6] = false;
                DataCenter.ModelUsingTexture[6] = false;
                DataCenter.ModelUsingUserSetTexture[6] = false;
            }
            else
            {
                if (modelMaterial.GetTexture(mapNames[i]) != null)
                {
                    DataCenter.ModelUsingGenericTexture[i] = false; // IF MODEL USES TEXTURE LIKE IN GAME
                    DataCenter.ModelUsingTexture[i] = true;
                    DataCenter.ModelUsingUserSetTexture[i] = false;
                }
                else
                {
                    modelMaterial.SetTexture(mapNames[i], genericDefaultTextures[i]);
                    DataCenter.ModelUsingGenericTexture[i] = true; // IF MODEL IS INSTEAD USING GENERIC TEXTURE
                    DataCenter.ModelUsingTexture[i] = true;
                    DataCenter.ModelUsingUserSetTexture[i] = false;

                }
                DataCenter.ModelUsingUserSetTexture[i] = false; // IF MODEL IS INSTEAD USING USER SET TEXTURE
            }
        }
    }

    // ENTRY POINT OF FILE ON MODEL INSTANTIATION
    public void ReferenceModelTextures(Material ModelMaterial)
    {
        // GETS ALL TEXTURES TO APPLY TO MODEL
        // List<Texture2D> modelDefaultTextures = getModelDefaultTextures(DataCenter.SelectedModelName);
        setModelDefaultTextures(ModelMaterial);

        EnableModelShaderKeywords(ModelMaterial);

        string[] mapNames = GetShaderMaps();

        ModelDefaultAlbedo = ModelMaterial.mainTexture;
        ModelDefaultEmissive = ModelMaterial.GetTexture(mapNames[7]);
        ModelDefaultNormal = ModelMaterial.GetTexture(mapNames[1]);
        ModelDefaultHeight = ModelMaterial.GetTexture(mapNames[5]);
        ModelDefaultDetail = ModelMaterial.GetTexture(mapNames[6]);
        if (DataCenter.ModelUsingTexture[2])
            ModelDefaultMetallic = ModelMaterial.GetTexture(DataCenter.UsingSpecularSetup ? "_SpecGlossMap" : "_MetallicGlossMap");
        ModelDefaultOcclusion = ModelMaterial.GetTexture(mapNames[4]);
        ModelDefaultGlossDetail2 = ModelMaterial.GetTexture(mapNames[3]);

        // STORES DEFAULT TEXTURES
        DataCenter.ModelAlbedoTexture = (Texture2D)ModelDefaultAlbedo;
        DataCenter.ModelEmissiveTexture = (Texture2D)ModelDefaultEmissive;
        DataCenter.ModelNormalMapTexture = (Texture2D)ModelDefaultNormal;
        DataCenter.ModelHeightMapTexture = (Texture2D)ModelDefaultHeight;
        DataCenter.ModelDetailMapTexture = (Texture2D)ModelDefaultDetail;
        DataCenter.ModelMetallicTexture = (Texture2D)ModelDefaultMetallic;
        DataCenter.ModelOcclusionTexture = (Texture2D)ModelDefaultOcclusion;
        DataCenter.ModelGlossDetail2Texture = (Texture2D)ModelDefaultGlossDetail2;

        DataCenter.ModelDefaultAlbedoTexture = (Texture2D)ModelDefaultAlbedo;
        DataCenter.ModelDefaultEmissiveTexture = (Texture2D)ModelDefaultEmissive;
        DataCenter.ModelDefaultNormalMapTexture = (Texture2D)ModelDefaultNormal;
        DataCenter.ModelDefaultHeightMapTexture = (Texture2D)ModelDefaultHeight;
        DataCenter.ModelDefaultDetailMapTexture = (Texture2D)ModelDefaultDetail;
        DataCenter.ModelDefaultMetallicTexture = (Texture2D)ModelDefaultMetallic;
        DataCenter.ModelDefaultOcclusionTexture = (Texture2D)ModelDefaultOcclusion;
        DataCenter.ModelDefaultGlossDetail2Texture = (Texture2D)ModelDefaultGlossDetail2;

        if (AlbedoTextureImage != null)
            AlbedoTextureImage.texture = NoTexture;
        if(GlossDetail2TextureImage != null)
            GlossDetail2TextureImage.texture = NoTexture;
        if (EmissiveTextureImage != null)
            EmissiveTextureImage.texture = NoTexture;
        if (HeightTextureImage != null)
            HeightTextureImage.texture = NoTexture;
        if (MetallicTextureImage != null)
            MetallicTextureImage.texture = NoTexture;
        if (NormalTextureImage != null)
            NormalTextureImage.texture = NoTexture;
        if (OcclusionTextureImage != null)
            OcclusionTextureImage.texture = NoTexture;
    }

    public void EnableModelShaderKeywords(Material ModelMaterial)
    {
        // TELLS SHADER TO ENABLE SHIT SO ALL MAPS DISPLAY IF NEEDED
        ModelMaterial.EnableKeyword("_NORMALMAP");
        ModelMaterial.EnableKeyword("_EMISSION");
        // ModelMaterial.SetColor("_EmissionColor", Color.white);
        ModelMaterial.EnableKeyword("_PARALLAXMAP");
        ModelMaterial.EnableKeyword("_DETAIL_MULX2");
        if(DataCenter.ModelUsingTexture[2])
            ModelMaterial.EnableKeyword(DataCenter.UsingSpecularSetup ? "_SPECGLOSSMAP" : "_METALLICGLOSSMAP");
        else
            ModelMaterial.DisableKeyword(DataCenter.UsingSpecularSetup ? "_SPECGLOSSMAP" : "_METALLICGLOSSMAP");
    }

    public string[] GetShaderMaps()
    {
        string[] mapNames = { "", "_BumpMap", "", "_DetailAlbedoMap" ,
            "_OcclusionMap", "_ParallaxMap", "_DetailMask", "_EmissionMap" };

        return mapNames;
    }

    [ContextMenu("Get Shader Keywords")]
    public void GetShaderKeywords()
    {
        // Get the instance of the Shader class that the material uses
        string[] shader = DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().material.shaderKeywords;
        foreach(string s in shader)
            Debug.Log(s);
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetString("DataCenter_LastFilePath", DataCenter.LastFilePath);
    }

    public void LoadTexturesFromWeaponSkinFolder()
    {
        string weaponSkinsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/Skillwarz/WeaponSkins";

        // If the document doesn't exist, create one.
        System.IO.Directory.CreateDirectory(weaponSkinsPath);

        // Now it should exist, so we can create and load files from it.
        // Getting the name of the current weapon and checking if it exists in
        // the WeaponSkins folder.
        string ingameName = DataCenter.InstantiatedModelSO.WeaponNameInGUI;

        // All files in WeaponSkins folder.
        string[] filesInPath = System.IO.Directory.GetFiles(weaponSkinsPath);

        for(int i = 0; i < filesInPath.Length; i++)
        {
            //If it's the selected weapon && if it's a .png
            if(System.IO.Path.GetFileNameWithoutExtension(filesInPath[i]).Contains(ingameName) && System.IO.Path.GetExtension(filesInPath[i]) == ".png")
            {
                // Weapon is valid. Keep it.
                string strictPath = System.IO.Path.GetFileName(filesInPath[i]);

                if(strictPath.Contains("_E")) /*EmissiveTexture Map*/
                {
                    DataCenter.TabsManagerScript.GetTabFromName("Emissive").gameObject.SetActive(true);

                    Texture2D t = new Texture2D(2,2);
                    t.LoadImage( System.IO.File.ReadAllBytes(filesInPath[i]), false );
                    t.Apply();

                    EmissiveTexture = t;
                    AddEmissive();

                    EmissiveTextureImage.texture = EmissiveTexture;
                }
                else if(strictPath.Contains("_M")) /*MetallicTexture Map*/
                {
                    DataCenter.TabsManagerScript.GetTabFromName("Metallic").gameObject.SetActive(true);
                    
                    Texture2D t = new Texture2D(2,2);
                    t.LoadImage( System.IO.File.ReadAllBytes(filesInPath[i]), false );
                    t.Apply();

                    MetallicTexture = t;
                    AddMetallic();

                    MetallicTextureImage.texture = MetallicTexture;
                }
                else if(strictPath.Contains("_N")) /*NormalTexture Map*/
                {
                    DataCenter.TabsManagerScript.GetTabFromName("Normal").gameObject.SetActive(true);
                    
                    Texture2D t = new Texture2D(2,2);
                    t.LoadImage( System.IO.File.ReadAllBytes(filesInPath[i]), false );
                    t.Apply();

                    NormalTexture = t;
                    AddNormal();

                    NormalTextureImage.texture = NormalTexture;
                }
                else if(strictPath.Contains("_D")) /*DetailTexture Map*/
                {
                    DataCenter.TabsManagerScript.GetTabFromName("Detail").gameObject.SetActive(true);
                    
                    Texture2D t = new Texture2D(2,2);
                    t.LoadImage( System.IO.File.ReadAllBytes(filesInPath[i]), false );
                    t.Apply();

                    DetailTexture = t;
                    AddDetail();

                    GlossDetail2TextureImage.texture = GlossDetail2Texture;
                }
                else if(strictPath.Contains("_H")) /*HeightTexture Map*/
                {
                    DataCenter.TabsManagerScript.GetTabFromName("Height").gameObject.SetActive(true);
                    
                    Texture2D t = new Texture2D(2,2);
                    t.LoadImage( System.IO.File.ReadAllBytes(filesInPath[i]), false );
                    t.Apply();

                    HeightTexture = t;
                    AddHeight();
                    
                    HeightTextureImage.texture = HeightTexture;
                }
                else if(strictPath.Contains("_O")) /*OcclusionTexture Map*/
                {
                    DataCenter.TabsManagerScript.GetTabFromName("Occlusion").gameObject.SetActive(true);
                    
                    Texture2D t = new Texture2D(2,2);
                    t.LoadImage( System.IO.File.ReadAllBytes(filesInPath[i]), false );
                    t.Apply();

                    OcclusionTexture = t;
                    AddOcclusion();

                    OcclusionTextureImage.texture = OcclusionTexture;
                }
                else /*Albedo*/
                {
                    DataCenter.TabsManagerScript.GetTabFromName("Albedo").gameObject.SetActive(true);
                    
                    Texture2D t = new Texture2D(2,2);
                    t.LoadImage( System.IO.File.ReadAllBytes(filesInPath[i]), false );
                    t.Apply();

                    AlbedoTexture = t;
                    AddAlbedo();

                    AlbedoTextureImage.texture = AlbedoTexture;
                }

            }
        }

    }

    public void ApplyTextureMapToModel(string loadedImagePath, DataCenter.MaterialFields materialType)
    {
        DataCenter.TabsManagerScript.GetTabFromName("Metallic").gameObject.SetActive(true);
                    
        Texture2D t = new Texture2D(2,2);
        t.LoadImage( System.IO.File.ReadAllBytes(loadedImagePath), false );
        t.Apply();

        MetallicTexture = t;
        AddAlbedo();

        MetallicTextureImage.texture = MetallicTexture;
    }

    #region FindTextures

    string GetPathWithoutFileName(string m_AlbedoPath)
    {
        if(m_AlbedoPath.Contains("\\"))
            return m_AlbedoPath.Substring(0, m_AlbedoPath.LastIndexOf('\\'));
        else
            return m_AlbedoPath.Substring(0, m_AlbedoPath.LastIndexOf('/'));
    }

    public void FindAlbedoTexture()
    {
        string m_AlbedoPath = StandaloneFileBrowser.OpenFilePanel("Select a Texture...", DataCenter.LastFilePath, "png", false)[0];

        if(!string.IsNullOrEmpty(m_AlbedoPath))
        {
            byte[] m_ImageBytes = System.IO.File.ReadAllBytes(m_AlbedoPath);

            print(m_AlbedoPath);
            Texture2D m_Texture = new Texture2D(2,2);
            m_Texture.LoadImage(m_ImageBytes);
            AlbedoLastPath = m_AlbedoPath;
            m_AlbedoPath = GetPathWithoutFileName(m_AlbedoPath);
            AlbedoTexture = m_Texture;
            DataCenter.LastFilePath = m_AlbedoPath;
            AlbedoTextureImage.texture = m_Texture;
        }
    }

    public void FindEmissiveTexture()
    {
        string m_AlbedoPath = StandaloneFileBrowser.OpenFilePanel("Select a Texture...", DataCenter.LastFilePath, "png", false)[0];

        if(!string.IsNullOrEmpty(m_AlbedoPath))
        {
            byte[] m_ImageBytes = System.IO.File.ReadAllBytes(m_AlbedoPath);
            
            Texture2D m_Texture = new Texture2D(2,2);
            m_Texture.LoadImage(m_ImageBytes);
            EmissiveLastPath = m_AlbedoPath;
            m_AlbedoPath = GetPathWithoutFileName(m_AlbedoPath);
            EmissiveTexture = m_Texture;
            DataCenter.LastFilePath = m_AlbedoPath;
            EmissiveTextureImage.texture = m_Texture;
        }
    }

    public void FindMetallicTexture()
    {
        string m_AlbedoPath = StandaloneFileBrowser.OpenFilePanel("Select a Texture...", DataCenter.LastFilePath, "png", false)[0];

        if(!string.IsNullOrEmpty(m_AlbedoPath))
        {
            byte[] m_ImageBytes = System.IO.File.ReadAllBytes(m_AlbedoPath);
            
            Texture2D m_Texture = new Texture2D(2,2);
            m_Texture.LoadImage(m_ImageBytes);
            MetallicLastPath = m_AlbedoPath;
            m_AlbedoPath = GetPathWithoutFileName(m_AlbedoPath);
            MetallicTexture = m_Texture;
            DataCenter.LastFilePath = m_AlbedoPath;
            MetallicTextureImage.texture = m_Texture;
        }
    }

    public void FindHeightTexture()
    {
        string m_AlbedoPath = StandaloneFileBrowser.OpenFilePanel("Select a Texture...", DataCenter.LastFilePath, "png", false)[0];

        if(!string.IsNullOrEmpty(m_AlbedoPath))
        {
            byte[] m_ImageBytes = System.IO.File.ReadAllBytes(m_AlbedoPath);
            
            Texture2D m_Texture = new Texture2D(2,2);
            m_Texture.LoadImage(m_ImageBytes);
            HeightLastPath = m_AlbedoPath;
            m_AlbedoPath = GetPathWithoutFileName(m_AlbedoPath);
            HeightTexture = m_Texture;
            DataCenter.LastFilePath = m_AlbedoPath;
            HeightTextureImage.texture = m_Texture;
        }
    }

    public void FindNormalTexture()
    {
        string m_AlbedoPath = StandaloneFileBrowser.OpenFilePanel("Select a Texture...", DataCenter.LastFilePath, "png", false)[0];

        if(!string.IsNullOrEmpty(m_AlbedoPath))
        {
            byte[] m_ImageBytes = System.IO.File.ReadAllBytes(m_AlbedoPath);
            
            Texture2D m_Texture = new Texture2D(2,2);
            m_Texture.LoadImage(m_ImageBytes);
            NormalLastPath = m_AlbedoPath;
            m_AlbedoPath = GetPathWithoutFileName(m_AlbedoPath);
            NormalTexture = m_Texture;
            DataCenter.LastFilePath = m_AlbedoPath;
            NormalTextureImage.texture = m_Texture;
        }
    }

    public void FindDetailTexture()
    {
        string m_AlbedoPath = StandaloneFileBrowser.OpenFilePanel("Select a Texture...", DataCenter.LastFilePath, "png", false)[0];

        if(!string.IsNullOrEmpty(m_AlbedoPath))
        {
            byte[] m_ImageBytes = System.IO.File.ReadAllBytes(m_AlbedoPath);
            
            Texture2D m_Texture = new Texture2D(2,2);
            m_Texture.LoadImage(m_ImageBytes);
            GlossDetail2LastPath = m_AlbedoPath;
            m_AlbedoPath = GetPathWithoutFileName(m_AlbedoPath);
            GlossDetail2Texture = m_Texture;
            DataCenter.LastFilePath = m_AlbedoPath;
            GlossDetail2TextureImage.texture = m_Texture;
        }
    }

    public void FindOcclusionTexture()
    {
        string m_AlbedoPath = StandaloneFileBrowser.OpenFilePanel("Select a Texture...", DataCenter.LastFilePath, "png", false)[0];

        if(!string.IsNullOrEmpty(m_AlbedoPath))
        {
            byte[] m_ImageBytes = System.IO.File.ReadAllBytes(m_AlbedoPath);
            
            Texture2D m_Texture = new Texture2D(2,2);
            m_Texture.LoadImage(m_ImageBytes);
            OcclusionLastPath = m_AlbedoPath;
            m_AlbedoPath = GetPathWithoutFileName(m_AlbedoPath);
            OcclusionTexture = m_Texture;
            DataCenter.LastFilePath = m_AlbedoPath;
            OcclusionTextureImage.texture = m_Texture;
        }
    }

    #endregion

    #region AddTextures

    private Texture2D AddTexture(Texture tex, string _name)
    {
        if(DataCenter.InstantiatedModel == null)
            return null;

        // DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().material.EnableKeyword(_name);

        // is this line needed anymore? see beginning of ReferenceModelTextures
        // DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().material.shaderKeywords = new string[4]{"_NORMALMAP","_METALLICGLOSSMAP","_EMISSION","_PARALLAXMAP"};

        DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().sharedMaterial.SetTexture(_name, tex);

        return (Texture2D)tex;
    }

    public void AddAlbedo() {
        DataCenter.ModelUsingTexture[0] = true;
        DataCenter.ModelUsingGenericTexture[0] = false;
        DataCenter.ModelUsingUserSetTexture[0] = true;
        DataCenter.ModelAlbedoTexture = AddTexture(AlbedoTexture, "_MainTex");
    }
    public void AddEmissive() {
        DataCenter.ModelUsingTexture[7] = true;
        DataCenter.ModelUsingGenericTexture[7] = false;
        DataCenter.ModelUsingUserSetTexture[7] = true;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.EnableKeyword("_EMISSION");
        DataCenter.ModelEmissiveTexture = AddTexture(EmissiveTexture, "_EmissionMap");
    }
    public void AddHeight() {
        DataCenter.ModelUsingTexture[5] = true;
        DataCenter.ModelUsingGenericTexture[5] = false;
        DataCenter.ModelUsingUserSetTexture[5] = true;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.EnableKeyword("_PARALLAXMAP");
        DataCenter.ModelHeightMapTexture = AddTexture(HeightTexture, "_ParallaxMap");
    }
    public void AddMetallic() {
        DataCenter.ModelUsingTexture[2] = true;
        DataCenter.ModelUsingGenericTexture[2] = false;
        DataCenter.ModelUsingUserSetTexture[2] = true;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.EnableKeyword(DataCenter.UsingSpecularSetup ? "_SPECGLOSSMAP" : "_METALLICGLOSSMAP");
        DataCenter.ModelMetallicTexture = AddTexture(MetallicTexture, DataCenter.UsingSpecularSetup ? "_SpecGlossMap" : "_MetallicGlossMap");
    }
    public void AddNormal() {
        DataCenter.ModelUsingTexture[1] = true;
        DataCenter.ModelUsingGenericTexture[1] = false;
        DataCenter.ModelUsingUserSetTexture[1] = true;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.EnableKeyword("_NORMALMAP");
        DataCenter.ModelNormalMapTexture = AddTexture(NormalTexture, "_BumpMap");
    }
    public void AddDetail() {
        DataCenter.ModelUsingTexture[3] = true;
        DataCenter.ModelUsingGenericTexture[3] = false;
        DataCenter.ModelUsingUserSetTexture[3] = true;
        DataCenter.ModelGlossDetail2Texture = AddTexture(GlossDetail2Texture, "_DetailAlbedoMap");
    }
    public void AddOcclusion() {
        DataCenter.ModelUsingTexture[4] = true;
        DataCenter.ModelUsingGenericTexture[4] = false;
        DataCenter.ModelUsingUserSetTexture[4] = true;
        DataCenter.ModelOcclusionTexture = AddTexture(OcclusionTexture, "_OcclusionMap");
    }


    #endregion

    #region RemoveTextures

    public void RemoveAllTextures()
    {
        if (AlbedoTextureImage != null)
            RemoveAlbedo();
        if (GlossDetail2Texture != null)
            RemoveDetail();
        if (EmissiveLastPath != null)
            RemoveEmissive();
        if (HeightTexture != null)
            RemoveHeight();
        if (MetallicTexture != null)
            RemoveMetallic();
        if (NormalTexture != null)
            RemoveNormal();
        if (OcclusionTexture != null)
            RemoveOcclusion();
    }

    private Texture RemoveTexture(Texture tex, string _name)
    {
        DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().sharedMaterial.SetTexture(_name, tex);

        return tex;
    }

    public void RemoveAlbedo()
    {
        DataCenter.ModelUsingTexture[0] = true;
        DataCenter.ModelUsingUserSetTexture[0] = false;
        ModelDefaultAlbedo = DataCenter.ModelDefaultAlbedoTexture;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.mainTexture = DataCenter.ModelDefaultAlbedoTexture;
        DataCenter.ModelAlbedoTexture = ModelDefaultAlbedo as Texture2D;
        AlbedoTexture = (Texture2D)RemoveTexture(ModelDefaultAlbedo, "_MainTex");
    }

    public void RemoveEmissive()
    {
        DataCenter.ModelUsingTexture[7] = true;
        DataCenter.ModelUsingUserSetTexture[7] = false;
        DataCenter.ModelEmissiveTexture = ModelDefaultEmissive as Texture2D;
        ModelDefaultEmissive = DataCenter.ModelDefaultEmissiveTexture;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.SetTexture("_EmissionMap", DataCenter.ModelDefaultEmissiveTexture);
        EmissiveTexture = (Texture2D)RemoveTexture(ModelDefaultEmissive, "_EmissionMap");
    }   

    public void RemoveMetallic()
    {
        DataCenter.ModelUsingTexture[2] = true;
        DataCenter.ModelUsingUserSetTexture[2] = false;
        DataCenter.ModelMetallicTexture = ModelDefaultMetallic as Texture2D;
        ModelDefaultMetallic = DataCenter.ModelDefaultMetallicTexture;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.SetTexture(DataCenter.UsingSpecularSetup ? "_SpecGlossMap" : "_MetallicGlossMap", DataCenter.ModelDefaultMetallicTexture);
        DataCenter.ModelMetallicTexture = ModelDefaultMetallic as Texture2D;
        MetallicTexture = (Texture2D)RemoveTexture(ModelDefaultMetallic, DataCenter.UsingSpecularSetup ? "_SpecGlossMap" : "_MetallicGlossMap");
    }

    public void RemoveDetail()
    {
        DataCenter.ModelUsingTexture[6] = true;
        DataCenter.ModelUsingUserSetTexture[6] = false;
        DataCenter.ModelDetailMapTexture = ModelDefaultDetail as Texture2D;
        ModelDefaultGlossDetail2 = DataCenter.ModelDefaultGlossDetail2Texture;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.SetTexture("_DetailAlbedoMap", DataCenter.ModelDefaultGlossDetail2Texture);
        DataCenter.ModelDefaultGlossDetail2Texture = ModelDefaultGlossDetail2 as Texture2D;
        GlossDetail2Texture = (Texture2D)RemoveTexture(ModelDefaultGlossDetail2, "_DetailAlbedoMap");
    }

    public void RemoveHeight()
    {
        DataCenter.ModelUsingTexture[5] = true;
        DataCenter.ModelUsingUserSetTexture[5] = false;
        DataCenter.ModelHeightMapTexture = ModelDefaultHeight as Texture2D;
        ModelDefaultHeight = DataCenter.ModelDefaultHeightMapTexture;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.SetTexture("_ParallaxMap", DataCenter.ModelDefaultHeightMapTexture);
        DataCenter.ModelHeightMapTexture = ModelDefaultHeight as Texture2D;
        HeightTexture = (Texture2D)RemoveTexture(ModelDefaultHeight, "_ParallaxMap");
    }

    public void RemoveNormal()
    {
        DataCenter.ModelUsingTexture[1] = true;
        DataCenter.ModelUsingUserSetTexture[1] = false;
        DataCenter.ModelNormalMapTexture = ModelDefaultNormal as Texture2D;
        ModelDefaultNormal = DataCenter.ModelDefaultNormalMapTexture;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.SetTexture("_BumpMap", DataCenter.ModelDefaultNormalMapTexture);
        DataCenter.ModelNormalMapTexture = ModelDefaultNormal as Texture2D;
        NormalTexture = (Texture2D)RemoveTexture(ModelDefaultNormal, "_BumpMap");
    }

    public void RemoveOcclusion()
    {
        DataCenter.ModelUsingTexture[4] = true;
        DataCenter.ModelUsingUserSetTexture[4] = false;
        DataCenter.ModelOcclusionTexture = ModelDefaultOcclusion as Texture2D;
        ModelDefaultOcclusion = DataCenter.ModelDefaultOcclusionTexture;
        DataCenter.InstantiatedModel.GetComponents<Renderer>()[0].material.SetTexture("_OcclusionMap", DataCenter.ModelDefaultOcclusionTexture);
        DataCenter.ModelOcclusionTexture = ModelDefaultOcclusion as Texture2D;
        OcclusionTexture = (Texture2D)RemoveTexture(ModelDefaultOcclusion, "_OcclusionMap");
    }

    #endregion

    #region RefreshTextures

    public void RefreshAllTextures()
    {
        if(DataCenter.ModelUsingUserSetTexture[0])
        {
            Texture2D m_Texture = new Texture2D(2, 2);
            byte[] AlbedoImageBytes = System.IO.File.ReadAllBytes(AlbedoLastPath);
            m_Texture.LoadImage(AlbedoImageBytes);
            AlbedoTexture = m_Texture;
            AlbedoTextureImage.texture = m_Texture;
            DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = AlbedoTexture;
            DataCenter.ModelAlbedoTexture = AlbedoTexture;
        }

        if (DataCenter.ModelUsingUserSetTexture[7])
        {
            Texture2D m_Texture = new Texture2D(2, 2);
            byte[] EmissiveImageBytes = System.IO.File.ReadAllBytes(EmissiveLastPath);
            m_Texture.LoadImage(EmissiveImageBytes);
            EmissiveTexture = m_Texture;
            EmissiveTextureImage.texture = m_Texture;
            DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().sharedMaterial.SetTexture("_EmissionMap", EmissiveTexture);
            DataCenter.ModelEmissiveTexture = EmissiveTexture;

        }

        if (DataCenter.ModelUsingUserSetTexture[2])
        {
            Texture2D m_Texture = new Texture2D(2, 2);
            byte[] MetallicImageBytes = System.IO.File.ReadAllBytes(MetallicLastPath);
            m_Texture.LoadImage(MetallicImageBytes);
            MetallicTexture = m_Texture;
            MetallicTextureImage.texture = m_Texture;
            DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().sharedMaterial.SetTexture(DataCenter.UsingSpecularSetup ? "_SpecGlossMap" : "_MetallicGlossMap", MetallicTexture);
            DataCenter.ModelMetallicTexture = MetallicTexture;
        }

        if (DataCenter.ModelUsingUserSetTexture[5])
        {
            Texture2D m_Texture = new Texture2D(2, 2);
            byte[] HeightImageBytes = System.IO.File.ReadAllBytes(HeightLastPath);
            m_Texture.LoadImage(HeightImageBytes);
            HeightTexture = m_Texture;
            HeightTextureImage.texture = m_Texture;
            DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().sharedMaterial.SetTexture("_ParallaxMap", HeightTexture);
            DataCenter.ModelHeightMapTexture = HeightTexture;
        }

        if (DataCenter.ModelUsingUserSetTexture[1])
        {
            Texture2D m_Texture = new Texture2D(2, 2);
            byte[] NormalImageBytes = System.IO.File.ReadAllBytes(NormalLastPath);
            m_Texture.LoadImage(NormalImageBytes);
            NormalTexture = m_Texture;
            NormalTextureImage.texture = m_Texture;
            DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().sharedMaterial.SetTexture("_BumpMap", NormalTexture);
            DataCenter.ModelNormalMapTexture = NormalTexture;
        }

        if (DataCenter.ModelUsingUserSetTexture[4])
        {
            Texture2D m_Texture = new Texture2D(2, 2);
            byte[] OcclusionImageBytes = System.IO.File.ReadAllBytes(OcclusionLastPath);
            m_Texture.LoadImage(OcclusionImageBytes);
            OcclusionTexture = m_Texture;
            OcclusionTextureImage.texture = m_Texture;
            DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().sharedMaterial.SetTexture("_OcclusionMap", OcclusionTexture);
            DataCenter.ModelOcclusionTexture = OcclusionTexture;
        }

        if (DataCenter.ModelUsingUserSetTexture[3])
        {
            Texture2D m_Texture = new Texture2D(2, 2);
            byte[] GlossDetail2ImageBytes = System.IO.File.ReadAllBytes(GlossDetail2LastPath);
            m_Texture.LoadImage(GlossDetail2ImageBytes);
            GlossDetail2Texture = m_Texture;
            GlossDetail2TextureImage.texture = m_Texture;
            DataCenter.InstantiatedModel.transform.GetComponent<MeshRenderer>().sharedMaterial.SetTexture("_DetailAlbedoMap", GlossDetail2Texture);
            DataCenter.ModelDetailMapTexture = GlossDetail2Texture;
        }
    }
    
    #endregion

    #region ExportTextures

    public void ExportAlbedo()
    {
        if (!DataCenter.ModelUsingUserSetTexture[0])
            return;

        string path = StandaloneFileBrowser.SaveFilePanel("Export your Texture...", System.IO.Path.GetDirectoryName(DataCenter.LastFilePath), DataCenter.SelectedModelName + "_" + "A", "png");

        if(!string.IsNullOrEmpty(path))
        {
            byte[] bytes = DataCenter.ModelAlbedoTexture.EncodeToPNG();
            System.IO.File.WriteAllBytes(path, bytes);
        }
    }

    public void ExportMetallic()
    {
        if (!DataCenter.ModelUsingUserSetTexture[2])
            return;

        string path = StandaloneFileBrowser.SaveFilePanel("Export your Texture...", System.IO.Path.GetDirectoryName(DataCenter.LastFilePath), DataCenter.SelectedModelName + "_" + "M", "png");

        if(!string.IsNullOrEmpty(path))
        {
            byte[] bytes = DataCenter.ModelMetallicTexture.EncodeToPNG();
            System.IO.File.WriteAllBytes(path, bytes);
        }
    }

    public void ExportNormal()
    {
        if (!DataCenter.ModelUsingUserSetTexture[1])
            return;

        string path = StandaloneFileBrowser.SaveFilePanel("Export your Texture...", System.IO.Path.GetDirectoryName(DataCenter.LastFilePath), DataCenter.SelectedModelName + "_" + "N", "png");

        if(!string.IsNullOrEmpty(path))
        {
            byte[] bytes = DataCenter.ModelNormalMapTexture.EncodeToPNG();
            System.IO.File.WriteAllBytes(path, bytes);
        }
    }

    public void ExportDetail()
    {
        if (!DataCenter.ModelUsingUserSetTexture[6])
            return;

        string path = StandaloneFileBrowser.SaveFilePanel("Export your Texture...", System.IO.Path.GetDirectoryName(DataCenter.LastFilePath), DataCenter.SelectedModelName + "_" + "D", "png");

        if(!string.IsNullOrEmpty(path))
        {
            byte[] bytes = DataCenter.ModelDetailMapTexture.EncodeToPNG();
            System.IO.File.WriteAllBytes(path, bytes);
        }
    }

    public void ExportOcclusion()
    {
        if (!DataCenter.ModelUsingUserSetTexture[4])
            return;

        string path = StandaloneFileBrowser.SaveFilePanel("Export your Texture...", System.IO.Path.GetDirectoryName(DataCenter.LastFilePath), DataCenter.SelectedModelName + "_" + "O", "png");

        if(!string.IsNullOrEmpty(path))
        {
            byte[] bytes = DataCenter.ModelOcclusionTexture.EncodeToPNG();
            System.IO.File.WriteAllBytes(path, bytes);
        }
    }

    public void ExportEmissive()
    {
        if (!DataCenter.ModelUsingUserSetTexture[7])
            return;

        string path = StandaloneFileBrowser.SaveFilePanel("Export your Texture...", System.IO.Path.GetDirectoryName(DataCenter.LastFilePath), DataCenter.SelectedModelName + "_" + "E", "png");

        if(!string.IsNullOrEmpty(path))
        {
            byte[] bytes = DataCenter.ModelEmissiveTexture.EncodeToPNG();
            System.IO.File.WriteAllBytes(path, bytes);
        }
    }

    public void ExportHeight()
    {
        if (!DataCenter.ModelUsingUserSetTexture[5])
            return;

        string path = StandaloneFileBrowser.SaveFilePanel("Export your Texture...", System.IO.Path.GetDirectoryName(DataCenter.LastFilePath), DataCenter.SelectedModelName + "_" + "H", "png");

        if(!string.IsNullOrEmpty(path))
        {
            byte[] bytes = DataCenter.ModelHeightMapTexture.EncodeToPNG();
            System.IO.File.WriteAllBytes(path, bytes);
        }
    }

    #endregion
}