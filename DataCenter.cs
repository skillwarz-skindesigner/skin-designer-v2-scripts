using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class DataCenter
{
    public const string CURRENT_SD_VERSION = "Beta3";
    public static string ReportBugLink = "";

    public enum TEXTURE_MAPS
    {
        Albedo, Normal, MetallicSpecular, GlossDetail2, Occlusion, Heightmap, ObsoleteDetail, Emission
    }

    public static string SkillwarzDocumentPath;

    public static GameObject InstantiatedModel;
    public static ModelSO InstantiatedModelSO;

    public static Texture2D ModelAlbedoTexture;
    public static Texture2D ModelNormalMapTexture;
    public static Texture2D ModelMetallicTexture;
    public static Texture2D ModelGlossDetail2Texture;
    public static Texture2D ModelOcclusionTexture;
    public static Texture2D ModelHeightMapTexture;
    public static Texture2D ModelDetailMapTexture;
    public static Texture2D ModelEmissiveTexture;

    public static Texture2D ModelDefaultAlbedoTexture;
    public static Texture2D ModelDefaultNormalMapTexture;
    public static Texture2D ModelDefaultMetallicTexture;
    public static Texture2D ModelDefaultGlossDetail2Texture;
    public static Texture2D ModelDefaultOcclusionTexture;
    public static Texture2D ModelDefaultHeightMapTexture;
    public static Texture2D ModelDefaultDetailMapTexture;
    public static Texture2D ModelDefaultEmissiveTexture;

    public static bool[] ModelUsingUserSetTexture = new bool[8];
    public static bool[] ModelUsingGenericTexture = new bool[8];
    public static bool[] ModelUsingTexture = new bool[8];


    public static string SelectedModelName;
    public static int SelectedModelListIndex = -1;
    public static bool SelectedModelIsAttachment;
    public static bool UsingSpecularSetup; // instead of Metallic Setup
    public static bool UsingAlbedoAlpha; // instead of Metallic Alpha, for Gloss

    public static Vector4 FreeSpace = new Vector4(500,847,1919,-49);

    public static ApplyTexturesToModel ApplyTexturesToModelScript;
    public static bool RendererIsAttachedToChild = false;

    public static bool IsUIWindowOpenned = false;
    public static bool AfterFirstProjectCreatedThisInstance = false;

    public static Texture2D SpriteToTexture2D(Sprite sprite)
    {
        Texture2D m_Texture2D = new Texture2D( (int)sprite.rect.width, (int)sprite.rect.height );

        var m_Pixels = sprite.texture.GetPixels(
            (int)sprite.textureRect.x,
            (int)sprite.textureRect.y,
            (int)sprite.textureRect.width,
            (int)sprite.textureRect.height
        );

        m_Texture2D.SetPixels(m_Pixels);
        m_Texture2D.Apply();

        return m_Texture2D;
    }

    public static bool IsMouseOutOfUI { get { return (
                Input.mousePosition.x > DataCenter.FreeSpace.x && Input.mousePosition.x < DataCenter.FreeSpace.z && 
                Input.mousePosition.y < DataCenter.FreeSpace.y && Input.mousePosition.y > DataCenter.FreeSpace.w
                ); } }

    public static bool UIDisableBecauseObjectDrag = false;
    public static bool OverrideUILogicEnableCameraView = false;
    public static bool IsMouseOutOfColorPickers = true;
    public static bool[] IsMouseOutOfUIObject = new bool[3];

    public static void Populate<T>(this T[] arr, T value)
    {
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = value;
        }
    }

    public static string LastFilePath = "./";
    public static short DefaultCameraFOV = 40;
    public static float CameraFOV = 40;
    public static Vector3 DefaultCameraPosition = new Vector3(0, 0, -1.5f);

    public static CustomButton CurrentTextureTab;
    public static TabsManager TabsManagerScript;

    public static bool FPSCounterVisible = true;
    public static bool RetardModeOn = false;
    public static short SavedCameraView = 0;

    public static OffsetModifier OffsetModifierScript;

    /*
    public static class TextureMapOptionsObjects
    {
        public static FlexibleColorPicker albedo;
        public static Slider metallic;
        public static FlexibleColorPicker spec;
        public static Slider glossiness;
        public static Slider glossmapscale;
        public static Slider bumpscale;
        public static Slider heightscale;
        public static Slider occlusionstrength;
        public static Slider emissionintensity;
        public static FlexibleColorPicker emission;
    }
    */

    public enum MaterialFields
    {
       [InspectorName("Albedo")]_A,
       [InspectorName("Metallic")]_M,
       [InspectorName("Specular")]_E,
       [InspectorName("Normal Map")]_N,
       [InspectorName("Detail Map")]_D,
       [InspectorName("Height Map")]_H,
       [InspectorName("Occlusion Map")]_O
    }

    string GetPathWithoutFileName(string m_AlbedoPath)
    {
        if(m_AlbedoPath.Contains("\\"))
            return m_AlbedoPath.Substring(0, m_AlbedoPath.LastIndexOf('\\'));
        else
            return m_AlbedoPath.Substring(0, m_AlbedoPath.LastIndexOf('/'));
    }

}